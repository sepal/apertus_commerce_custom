<?php
/**
 * @file
 * apertus_commerce_custom.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function apertus_commerce_custom_default_rules_configuration() {
  $items = array();
  $items['commerce_tax_rate_example_payment_fee'] = entity_import('rules_config', '{ "commerce_tax_rate_example_payment_fee" : {
      "LABEL" : "Calculate Example payment fee",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Tax", "payment_fee" ],
      "REQUIRES" : [ "commerce_payment", "commerce_tax" ],
      "USES VARIABLES" : { "commerce_line_item" : { "type" : "commerce_line_item", "label" : "Line item" } },
      "IF" : [
        { "commerce_payment_selected_payment_method" : {
            "commerce_order" : [ "commerce-line-item:order" ],
            "method_id" : "commerce_payment_example"
          }
        }
      ],
      "DO" : [
        { "commerce_tax_rate_apply" : {
            "USING" : {
              "commerce_line_item" : [ "commerce-line-item" ],
              "tax_rate_name" : "example_payment_fee"
            },
            "PROVIDE" : { "applied_tax" : { "applied_tax" : "Applied tax" } }
          }
        }
      ]
    }
  }');
  $items['commerce_tax_rate_paypal_fee'] = entity_import('rules_config', '{ "commerce_tax_rate_paypal_fee" : {
      "LABEL" : "Calculate Paypal fee",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Tax", "payment_fee" ],
      "REQUIRES" : [ "commerce_payment", "commerce_tax" ],
      "USES VARIABLES" : { "commerce_line_item" : { "type" : "commerce_line_item", "label" : "Line item" } },
      "IF" : [
        { "commerce_payment_selected_payment_method" : {
            "commerce_order" : [ "commerce-line-item:order" ],
            "method_id" : "paypal_wps"
          }
        }
      ],
      "DO" : [
        { "commerce_tax_rate_apply" : {
            "USING" : {
              "commerce_line_item" : [ "commerce-line-item" ],
              "tax_rate_name" : "paypal_fee"
            },
            "PROVIDE" : { "applied_tax" : { "applied_tax" : "Applied tax" } }
          }
        }
      ]
    }
  }');
  $items['commerce_tax_rate_vat_at'] = entity_import('rules_config', '{ "commerce_tax_rate_vat_at" : {
      "LABEL" : "Calculate VAT AT",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Tax", "vat" ],
      "REQUIRES" : [ "commerce_tax" ],
      "USES VARIABLES" : { "commerce_line_item" : { "type" : "commerce_line_item", "label" : "Line item" } },
      "DO" : [
        { "commerce_tax_rate_apply" : {
            "USING" : {
              "commerce_line_item" : [ "commerce-line-item" ],
              "tax_rate_name" : "vat_at"
            },
            "PROVIDE" : { "applied_tax" : { "applied_tax" : "Applied tax" } }
          }
        }
      ]
    }
  }');
  $items['rules_apertus_limit_camera_quantity'] = entity_import('rules_config', '{ "rules_apertus_limit_camera_quantity" : {
      "LABEL" : "Limit Camera Quantity",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Apertus Commerce", "Commerce Cart" ],
      "REQUIRES" : [ "rules", "commerce_cart", "entity" ],
      "ON" : { "commerce_cart_product_add" : [], "commerce_line_item_update" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "commerce-product:type" ], "value" : "product" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "commerce-line-item:quantity" ], "value" : "1" } }
      ]
    }
  }');
  $items['rules_apertus_order_has_camera_product'] = entity_import('rules_config', '{ "rules_apertus_order_has_camera_product" : {
      "LABEL" : "Order has camera product",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "Apertus Commerce" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "product" : { "label" : "Product", "type" : "commerce_product" },
        "user" : { "label" : "User", "type" : "user" }
      },
      "IF" : [ { "data_is" : { "data" : [ "product:type" ], "value" : "product" } } ],
      "DO" : [
        { "data_set" : { "data" : [ "user:field-bought-camera" ], "value" : 1 } }
      ]
    }
  }');
  $items['rules_apertus_set_profile_bought_camera'] = entity_import('rules_config', '{ "rules_apertus_set_profile_bought_camera" : {
      "LABEL" : "Set profile bought camera",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Apertus Commerce" ],
      "REQUIRES" : [ "rules", "commerce_checkout" ],
      "ON" : { "commerce_checkout_complete" : [] },
      "DO" : [
        { "entity_fetch" : {
            "USING" : { "type" : "user", "id" : [ "commerce-order:uid" ] },
            "PROVIDE" : { "entity_fetched" : { "user" : "user" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "line_item" : "Current line item" },
            "DO" : [
              { "entity_query" : {
                  "USING" : {
                    "type" : "commerce_product",
                    "property" : "sku",
                    "value" : [ "line-item:line-item-label" ],
                    "limit" : "1"
                  },
                  "PROVIDE" : { "entity_fetched" : { "line_item_product" : "line_item_product" } }
                }
              },
              { "component_rules_apertus_order_has_camera_product" : { "product" : [ "line-item-product:0" ], "user" : [ "user" ] } }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_remove_product_if_type'] = entity_import('rules_config', '{ "rules_remove_product_if_type" : {
      "LABEL" : "Remove product if type",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "line_item" : { "label" : "Line item", "type" : "commerce_line_item" },
        "product_item" : { "label" : "Product item", "type" : "commerce_product" },
        "line_item_list" : {
          "label" : "Line item list",
          "type" : "list\\u003Ccommerce_line_item\\u003E"
        },
        "new_product_item_label" : { "label" : "New product item label", "type" : "text" }
      },
      "IF" : [
        { "data_is" : { "data" : [ "product-item:type" ], "value" : "product" } }
      ],
      "DO" : [
        { "list_remove" : { "list" : [ "line-item-list" ], "item" : [ "line-item" ] } },
        { "drupal_message" : { "message" : "Note: You are only allowed to buy one camera body. Therefore the old camera body \\u0022[product-item:title]\\u0022 was swapped for the new one \\u0022[new-product-item-label:value]\\u0022." } }
      ]
    }
  }');
  $items['rules_swap_line_item_product'] = entity_import('rules_config', '{ "rules_swap_line_item_product" : {
      "LABEL" : "Swap line item product",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Apertus Commerce" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : { "commerce_cart_product_prepare" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "commerce-product:type" ], "value" : "product" } }
      ],
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "line_item" : "Current line item" },
            "DO" : [
              { "entity_query" : {
                  "USING" : {
                    "type" : "commerce_product",
                    "property" : "sku",
                    "value" : [ "line-item:line-item-label" ],
                    "limit" : "1"
                  },
                  "PROVIDE" : { "entity_fetched" : { "current_product" : "Current product" } }
                }
              },
              { "component_rules_remove_product_if_type" : {
                  "line_item" : [ "line_item" ],
                  "product_item" : [ "current-product:0" ],
                  "line_item_list" : [ "commerce-order:commerce-line-items" ],
                  "new_product_item_label" : [ "commerce-product:title" ]
                }
              }
            ]
          }
        }
      ]
    }
  }');
  return $items;
}
