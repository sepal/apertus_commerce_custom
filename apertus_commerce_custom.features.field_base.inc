<?php
/**
 * @file
 * apertus_commerce_custom.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function apertus_commerce_custom_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_bought_camera'
  $field_bases['field_bought_camera'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_bought_camera',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'no',
        1 => 'yes',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  return $field_bases;
}
