<?php
/**
 * @file
 * apertus_commerce_custom.features.inc
 */

/**
 * Implements hook_commerce_tax_default_rates().
 */
function apertus_commerce_custom_commerce_tax_default_rates() {
  $items = array(
    'example_payment_fee' => array(
      'name' => 'example_payment_fee',
      'display_title' => 'Example payment fee',
      'description' => '',
      'rate' => 0.01,
      'type' => 'payment_fee',
      'rules_component' => 'commerce_tax_rate_example_payment_fee',
      'default_rules_component' => 1,
      'price_component' => 'tax|example_payment_fee',
      'calculation_callback' => 'commerce_tax_rate_calculate',
      'module' => 'commerce_tax_ui',
      'title' => 'Example payment fee',
      'admin_list' => TRUE,
    ),
    'paypal_fee' => array(
      'name' => 'paypal_fee',
      'display_title' => 'Paypal fee',
      'description' => '',
      'rate' => 0.04,
      'type' => 'payment_fee',
      'rules_component' => 'commerce_tax_rate_paypal_fee',
      'default_rules_component' => 1,
      'price_component' => 'tax|paypal_fee',
      'calculation_callback' => 'commerce_tax_rate_calculate',
      'module' => 'commerce_tax_ui',
      'title' => 'Paypal fee',
      'admin_list' => TRUE,
    ),
    'vat_at' => array(
      'name' => 'vat_at',
      'display_title' => 'VAT applicable to users from Austria.',
      'description' => '',
      'rate' => '0.20',
      'type' => 'vat',
      'rules_component' => 'commerce_tax_rate_vat_at',
      'default_rules_component' => 1,
      'price_component' => 'tax|vat_at',
      'calculation_callback' => 'commerce_tax_rate_calculate',
      'module' => 'commerce_tax_ui',
      'title' => 'VAT AT',
      'admin_list' => TRUE,
    ),
  );
  return $items;
}

/**
 * Implements hook_commerce_tax_default_types().
 */
function apertus_commerce_custom_commerce_tax_default_types() {
  $items = array(
    'payment_fee' => array(
      'name' => 'payment_fee',
      'display_title' => 'Payment fee',
      'description' => '',
      'display_inclusive' => 0,
      'round_mode' => 1,
      'rule' => 'commerce_tax_type_payment_fee',
      'module' => 'commerce_tax_ui',
      'title' => 'Payment fee',
      'admin_list' => TRUE,
    ),
    'vat' => array(
      'name' => 'vat',
      'display_title' => 'VAT',
      'description' => 'A basic type for taxes that display inclusive with product prices.',
      'display_inclusive' => 1,
      'round_mode' => 1,
      'rule' => 'commerce_tax_type_vat',
      'module' => 'commerce_tax_ui',
      'title' => 'VAT',
      'admin_list' => TRUE,
    ),
  );
  return $items;
}
