<?php
/**
 * @file
 * apertus_commerce_custom.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function apertus_commerce_custom_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'user-user-field_bought_camera'
  $field_instances['user-user-field_bought_camera'] = array(
    'bundle' => 'user',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_bought_camera',
    'label' => 'Bought camera',
    'required' => 0,
    'settings' => array(
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
      ),
      'type' => 'options_onoff',
      'weight' => 7,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Bought camera');

  return $field_instances;
}
